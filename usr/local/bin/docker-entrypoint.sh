#!/bin/bash

set -e

if [ "$1" = 'postfix' ]; then
    newaliases
    postmap /etc/postfix/sasl_passwd
    exec postfix -c /etc/postfix start-fg
fi

exec "$@"