#########################
# Create base container #
#########################
FROM ubuntu:24.04 as base
LABEL maintainer="pidydx"

# Base setup
ENV APP_USER=postfix
ENV APP_GROUP=postfix

ENV BASE_PKGS postfix postfix-ldap postfix-policyd-spf-perl libsasl2-modules

# Update users and groups
RUN userdel ubuntu
RUN groupadd -g 1000 ${APP_GROUP} \
 && useradd -r -M -N -u 1000 ${APP_USER} -g ${APP_GROUP} -s /usr/sbin/nologin


# Update and install base packages
RUN apt-get update -q \
 && DEBIAN_FRONTEND=noninteractive apt-get upgrade -yq \
 && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends ca-certificates \
 && apt-get update -q \
 && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends ${BASE_PKGS} \
 && rm -rf /var/lib/apt/lists/*


##########################
# Create final container #
##########################
FROM base

# Prepare container
COPY etc/ /etc/
COPY usr/ /usr/

EXPOSE 25/tcp 587/tcp 2525/tcp 5587/tcp
VOLUME ["/etc/postfix", "/var/spool/postfix"]

ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["postfix"]
